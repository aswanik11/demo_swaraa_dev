<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Models\User;
use App\Repositories\EloquentRepositories\UserRepository as Users;

class UserController extends Controller
{

    protected $request;
    protected $data;
    protected $user;

    public function __construct(Request $request, Users $user)
    {

        $this->request = $request;
        $this->user = $user;
        $this->data = [];
    }


    public function index()
    {
        $response['status'] = true;
        $response['user'] = $this->user->getUserList();
        // dd($userList);

        return response()->json($response, 200);
    }
}
