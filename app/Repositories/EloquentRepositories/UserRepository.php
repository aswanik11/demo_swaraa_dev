<?php

namespace App\Repositories\EloquentRepositories;

use App\Repositories\Contracts\RepositoryInterface;
use App\Repositories\Eloquent\Repository;
use DB;
use Exception;
use Auth;
use App\Models\ContestAwardData;
use session;
use App\Models\UserNotifications;
use Carbon\Carbon;

class UserRepository extends Repository
{

    public function model()
    {
        return 'App\Models\User';
    }

    public function getUserList()
    {
        // $user_id = ($user_id == null && isset(Auth::user()->id)) ? Auth::user()->id : $user_id;
        // //    dd($user_id);
        $query = DB::table('users')
            ->select('*')->get();

        return $query ? $query : false;
    }
}
