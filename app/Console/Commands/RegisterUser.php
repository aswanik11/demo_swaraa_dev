<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Faker\Factory as Faker;

class RegisterUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'register:user';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    // public function handle()
    // {
    //     return 0;
    // }
    public function handle()
    {
        $faker = \Faker\Factory::create();
        $user = new User();
        $user->password = Hash::make('the-password-of-choice');
        $user->email = $faker->unique()->safeEmail();
        $user->name = $faker->name();
        $user->email_verified_at = now();
        $user->remember_token = Str::random(10);

        $user->save();
    }
}
